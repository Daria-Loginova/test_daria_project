from django.contrib import admin

from .models import Post, Category, Tag, Quote,Author,Portfolio, FormMessage,Skills, Meta

admin.site.register(Post)
admin.site.register(Category)
admin.site.register(Tag)
admin.site.register(Quote)
admin.site.register(Author)
admin.site.register(Portfolio)




@admin.register(FormMessage)
class ContactFormAdmin(admin.ModelAdmin):
    pass


@admin.register(Skills)
class SkillAdmin(admin.ModelAdmin):
    list_display = ('title', 'value')
    list_display_links =  ('title',)
    list_editable = ('value',)



@admin.register(Meta)
class MetaAdmin(admin.ModelAdmin):
    list_display = ('key', 'value')
    list_editable = ('value',)
