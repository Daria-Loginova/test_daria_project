from django.urls import path
from .views import homepage, print_context, Login,BlogSingle,Register
from django.views.generic import  DetailView
from .models import Post
# http://127.0.0.1:8000/admin/
urlpatterns = [
    path('', homepage),
    path('index.html/', homepage,name='index'),

    path('context/', print_context, name='context'),
    path('login-new-user/', Login.as_view(), name='login'),
    path('register-new-user/', Register.as_view(), name='register'),
    path('blog/<int:pk>',BlogSingle.as_view(),name='blog-single'),
    path('login/',BlogSingle.as_view(),name='blog-single'),

   # path('blog/<int:pk>',DetailView.as_view(template_name = 'portfolio/blog-single.html',model=Post))
]
