# from django.http  import HttpResponse,HttpResponseRedirect
# import requests
# def get_google():
#     response = requests.get('http://google.com')
#     return HttpResponse(response)
# # def homepage(request):
# #     response1 = requests.get('http://google.com')
# #     return HttpResponse(response1)
# # Create your views here.
# #

import os
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from .models import Post, Quote, Portfolio, FormMessage, Skills, Meta
from portfolio.forms import ContactFormHTML
from .forms import RegisterForm, UserCreationForm
from django.contrib.auth import login, authenticate
from django.shortcuts import redirect, get_object_or_404
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import logout
from django.views.generic import DetailView, FormView
from django.urls import reverse_lazy


# from django.forms import F
def homepage(request):
    posts = Post.objects.all()
    qoutes = Quote.objects.all()
    portfolio_blanks = Portfolio.objects.all()
    contact_form = ContactFormHTML
    skills = Skills.objects.all()
    #about_text = Meta.objects.get(key='about_text').value
    if request.method == "POST":
        contact_form = ContactFormHTML(request.POST)
        if contact_form.is_valid():
            contact_form.save()
            # FormMessage(
            #     name = contact_form.cleaned_data['name'],
            #     email = contact_form.cleaned_data['email'],
            #     subject = contact_form.cleaned_data['subject'],
            #     message = contact_form.cleaned_data['message'],
            # ).save()
    # im = os.open(r'portfolio/assets/img/mypic.jpg')

    context = {
        'full_name': 'Daria Loginova',
        'job_list': ["python", "Java-script", "HTML", "CSS", ],
        'contact': {
            'phone': '+380936281036',
            'email': 'dashkalogy@gmail.com',
            #

        },
        # "about_text": about_text,
        'skills': skills,
        'portfolio_blanks': portfolio_blanks,
        'quotes': qoutes,
        'posts': posts,
        'contact_form': contact_form,
        'profile': "FullStack",
        'contact_info': {
            'location': 'Kiev',
            'phone': '+380936281036',
            'email': 'dashkalogy@gmail.com'
        },

    }

    return render(request, 'portfolio/index.html', context)


class Login(FormView):
    template_name = 'portfolio/login.html'
    success_url = reverse_lazy("login")
    form_class = AuthenticationForm

    def form_valid(self, form):
        user = form.get_user()
        login(self.request, user)
        return super().form_valid(form)


# def login_page(request):
#     form = AuthenticationForm()
#     if request.method == 'POST':
#
#         form = AuthenticationForm(data=request.POST)
#
#         if form.is_valid():
#             user = form.get_user()
#
#             login(request=request, user=user)
#
#     context = {
#         'form': form,
#     }
#
#     return render(request, 'portfolio/login.html', context)


# class LoginPage(FormView):
#     form = RegisterForm()
#     template_name = 'portfolio/register.html'
#     success_url = reverse_lazy('login')
#     def form_valid(self, form):
#         user = form.get_user()
#         login(request=self.request, user=user)
#         return  super().form_valid()
#

# def register_page(request):
#     form = RegisterForm()
#     if request.method == 'POST':
#         form = RegisterForm(request.POST)
#         if form.is_valid():
#             form.save()
#             return redirect('login')
#     context = {
#         'form': form,
#     }
#     return render(request, 'portfolio/register.html', context)
class Register(FormView):
    form_class = UserCreationForm
    template_name = "portfolio/register.html"
    success_url = reverse_lazy("login")

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)


def print_context(request):
    context = {
        'friends': ['leo', None, 'kate', 'sergio', 'Pavel', 'Daria'],
    }

    return render(request, 'portfolio/context.html', context)


def lout(request):
    logout(request)
    return HttpResponseRedirect("login")


# def blog_single(request,pk):
#     # post = Post.objects.get(pk=pk)
#     post = get_object_or_404(Post,pk=pk)
#     context = {
#         'post':post,
#     }
#     return render(request,'portfolio/blog-single.html',context)
#


class BlogSingle(DetailView):
    template_name = 'portfolio/blog-single.html'
    model = Post
