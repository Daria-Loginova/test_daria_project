from django.contrib.auth.models import User
from django.db import models


# ORM - Object Relationship Mapping
class Category(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class Tag(models.Model):
    title = models.CharField(max_length=20)

    def __str__(self):
        return self.title


# Создаем первую таблицу
class Post(models.Model):
    title = models.CharField(max_length=150)
    post_date = models.DateTimeField(auto_now_add=True)
    text = models.TextField()
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    tag = models.ForeignKey(Tag, on_delete=models.CASCADE, blank=True, null=True)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    image = models.ImageField(upload_to='uploads/%Y/%m/%d/', blank=True, )

    def __str__(self):
        return self.title


class Author(models.Model):
    name = models.CharField(max_length=25)
    surname = models.CharField(max_length=50)
    date = models.DateField(auto_now=False)

    def __str__(self):
        return self.name


class Quote(models.Model):
    quote_title = models.CharField(max_length=50)
    text = models.CharField(max_length=150)
    author = models.ForeignKey(Author, on_delete=models.CASCADE)

    def __str__(self):
        return self.quote_title


class Portfolio(models.Model):
    name = models.CharField(max_length=150)
    coding = models.CharField(max_length=150)
    date = models.DateField(auto_now=False)
    image = models.ImageField(upload_to='uploads/%Y/%m/%d/', blank=True, )


class FormMessage(models.Model):
    name = models.CharField(max_length=50)
    email = models.EmailField()
    subject = models.CharField(max_length=400)
    message = models.TextField()
    date_time = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.subject


class Skills(models.Model):
    title = models.CharField(max_length=31)
    value = models.IntegerField()

    def __str__(self):
        return self.title


class Meta(models.Model):
    key = models.CharField(max_length=32, unique=True)
    value = models.TextField()

    def __str__(self):
        return self.key
    class Meta:
        verbose_name_plural = "Meta"
