from django import forms
from .models import FormMessage
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib.auth import (
    authenticate, get_user_model, password_validation,
)

# class ContactFromHTML(forms.Form):
#     name = forms.CharField(max_length=300,widget=forms.TextInput(attrs={'class':'form-control'}))
#     email = forms.EmailField(widget=forms.EmailInput(attrs={'class':'form-control'}))
#     subject = forms.CharField(max_length=400,widget=forms.TextInput(attrs={'class':'form-control'}))
#     message = forms.CharField(widget=forms.Textarea(attrs={'class':'form-control'}))


class ContactFormHTML(forms.ModelForm):
    class Meta:
        model =FormMessage
        fields = '__all__'
        widgets = {
            'name':forms.TextInput(attrs={'class':'form-control','placeholder': 'name'}),
            'email' : forms.EmailInput(attrs={'class':'form-control', 'placeholder': 'email'}),
            'subject' :forms.TextInput(attrs={'class':'form-control', 'placeholder': 'subjec'}),
            'message': forms.Textarea(attrs={'class':'form-control', 'placeholder': 'message'}),
        }


class RegisterForm(UserCreationForm):
    password1 = forms.CharField(max_length=100, strip=False,widget=forms.PasswordInput(attrs={'autocomplete': 'new-password'}),)
    password2 = forms.CharField(max_length=100, strip=False,widget=forms.PasswordInput(attrs={'autocomplete': 'new-password'}), )
    username = forms.CharField(max_length=100)
    class Meta:
        model = User
        fields = ("username", "email", "password1", "password2")

    def save(self, commit=True):
        user = super(RegisterForm, self).save(commit=False)
        user.email = self.cleaned_data["email"]
        if commit:
            user.save()
        return user
